#include "TextSerialCom.h"
#include "Logger.h"

#include <fcntl.h>
#include <unistd.h>
#include <termios.h>

#include <cerrno>
#include <cstring>
#include <stdexcept>

//Register com
#include "ComRegistry.h"
REGISTER_COM(TextSerialCom)

TextSerialCom::TextSerialCom(const std::string& port, speed_t baud, bool parityBit, bool twoStopBits, bool flowControl, CharSize charsize)
  : SerialCom(port, baud, parityBit, twoStopBits, flowControl, charsize)
{ }

TextSerialCom::TextSerialCom()
  : SerialCom()
{ }

void TextSerialCom::setTermination(const std::string& termination)
{ m_termination=termination; }

std::string TextSerialCom::termination() const
{ return m_termination; }

void TextSerialCom::setConfiguration(const nlohmann::json& config)
{
  SerialCom::setConfiguration(config);

  for (const auto &kv : config.items())
    {
      if(kv.key()=="termination")
	{
	  m_termination=kv.value();
	}
    }
}

void TextSerialCom::send(const std::string& buf)
{
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << buf;
  SerialCom::send(buf+m_termination);
}

void TextSerialCom::send(char *buf, size_t length)
{
  send(std::string(buf,length));
}

std::string TextSerialCom::receive()
{
  lock();
  std::string buf;
  do
    {
      buf+=SerialCom::receive();
    }
  while(buf.size()<m_termination.size() || buf.substr(buf.size()-m_termination.size())!=m_termination);
  unlock();

  // rstrip termination
  buf=buf.substr(0,buf.size()-m_termination.size());

  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Received: " << buf;

  return buf;
}

uint32_t TextSerialCom::receive(char *buf, size_t length)
{
  lock();
  uint32_t n_read=0;
  do
    {
      n_read+=SerialCom::receive(buf+n_read, length-n_read);
    }
  while(n_read<m_termination.size() || strncmp(buf+n_read-m_termination.size(), m_termination.c_str(), m_termination.size())!=0);
  unlock();

  // rstrip new lines from end
  n_read-=m_termination.size();

  return n_read;
}
