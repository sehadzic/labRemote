#ifndef SERIALCOM_H
#define SERIALCOM_H

#include <unordered_map>
#include <string>
#include <termios.h>

#include "ICom.h"

/** 
 * Implementation of block serial communication with POSIX
 * receive/send calls.
 *
 */
class SerialCom : public ICom
{
public:
  /** Available character sizes */
  enum CharSize {CharSize5=CS5,CharSize6=CS6,CharSize7=CS7,CharSize8=CS8};
  
public:
  /** \brief Create serial communication object and set settings
   * @param port Device corresponding to the serial port
   * @param baud Baud rate to use
   * @param useParityBit Use parity bit
   * @param twoStopBits Use two stop bits instead of one
   * @param flowControl Enable hardware flow control
   * @param charsize Size of a character
   */
  SerialCom(const std::string& port, speed_t baud=B19200, bool parityBit=false, bool twoStopBits=false, bool flowControl=false, CharSize charsize=CharSize::CharSize8);
  SerialCom();

  ~SerialCom();

  /** Open device and configure
   */
  virtual void init();

  /** \brief Configure serial device based on JSON object
   *
   * Valid keys:
   *  - `device`: Path to serial device
   *  - `baudrate`: Baud rate (default: `"B19200"`)
   *  - `parityBit`: Enable parity bit (default: `false`)
   *  - `twoStopBits`: Use two stop bits instead of one (default: `false`)
   *  - `flowControl`: Enable hardware flow control (default: `false`)
   *  - `charsize`: Size of a character (default: `"C8"`)
   *
   * \param config JSON configuration
   */
  virtual void setConfiguration(const nlohmann::json& config);

  /** Send data to device
   *
   * Throw `std::runtime_error` on error.
   *
   * \param buf Data to be sent
   * \param length Number of characters in `buf` that should be sent
   */  
  virtual void send(char *buf, size_t length);

  /** Send data to device
   *
   * Throw `std::runtime_error` on error.
   *
   * \param buf Data to be sent
   */
  virtual void send(const std::string& buf);

  /** Read data from device
   *
   * Throw `std::runtime_error` on error.
   *
   * \return Received data
   */  
  virtual std::string receive();

  /** Read data from device
   *
   * Throw `std::runtime_error` on error.
   *
   * \param buf Buffer where to store results 
   * \param length Number of characters to receive.
   *
   * \return Number of characters received
   */
  virtual uint32_t receive(char *buf, size_t length);

  /** Write data to device and receive reply
   *
   * Throw `std::runtime_error` on error.
   *
   * \param cmd Data to be sent
   *
   * \return Returned data
   */
  virtual std::string sendreceive(const std::string& cmd);

  /** Write data to device and receive reply
   *
   * Throw `std::runtime_error` on error.
   *
   * \param wbuf Data to be sent
   * \param wlength Number of characters in `wbuf` that should be sent
   * \param rbuf Buffer where to store results 
   * \param rlength Number of characters to receive.
   *
   * \return Number of characters received
   */
  virtual void sendreceive(char *wbuf, size_t wlength, char *rbuf, size_t rlength);
  
  /** Flush any data in the device buffers
   */
  void flush();

  /** Toggle the Data Terminal Ready pin
   *
   * \param on New DTR value
   */
  void setDTR(bool on);

  /** Request exlusive access to device.
   *
   * If a single hardware bus is used to connect multiple devices,
   * the access to all of them should be locked to remove changes
   * of cross-talk.
   *
   * Throw `std::runtime_error` on error.
   *
   */  
  virtual void lock();

  /** Release exlusive access to device.
   *
   * Throw `std::runtime_error` on error.
   *
   */  
  virtual void unlock();

private:

  /** \brief Configure device for serial communication
   */
  void config();

  /** Configuration @{ */

  /// Name of device
  std::string m_port;

  /// Baud rate to use
  speed_t m_baudrate = B19200;

  /// Use parity bit
  bool m_parityBit=false;

  // Use two stop bits intead of one
  bool m_twoStopBits=false;

  // Enable hardware flow control
  bool m_flowControl=false;

  // Size of a character
  CharSize m_charsize=CharSize::CharSize8;

  /** @} */

  /** Run-time variables @{ */

  //! File handle for communication
  int m_dev =0;

  //! Current settings
  struct termios m_tty;

  //! Settings frm before config
  struct termios m_tty_old;

  //! Count number of lock() calls on this device
  uint32_t m_lock_counter = 0;

  /** @} */

  //! Maximum number of characters to read using string buffers
  static const unsigned MAX_READ = 4096;

  //! Temporary buffer for ::read()
  char m_tmpbuf[MAX_READ];

  //! Maps valid baud rate settings to `speed_t` type
  static const std::unordered_map<std::string, speed_t> mapBAUDRATE;

  //! Maps valid char size settings to `CharSize` type
  static const std::unordered_map<std::string, SerialCom::CharSize> mapCHARSIZE;
};

#endif
