# add executables
file(GLOB examples [a-zA-Z]*.cpp)

foreach(target ${examples})
  get_filename_component(execname ${target} NAME_WE)
  get_filename_component(srcfile ${target} NAME)

  add_executable(${execname})
  target_sources(${execname} PRIVATE ${srcfile})
  target_link_libraries(${execname} DevCom)
endforeach()
