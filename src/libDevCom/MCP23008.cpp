#include "MCP23008.h"

MCP23008::MCP23008(std::shared_ptr<I2CCom> com)
  : IOExpander(), m_com(com)
{
  m_com->write_reg8(MCP23008_IPOL, 0x0); // Do not support inversion
}

uint32_t MCP23008::getIO()
{
  return m_com->read_reg8(MCP23008_IODIR);
}

void MCP23008::setIO(uint32_t input)
{
  m_com->write_reg8(MCP23008_IODIR, input);
}

void MCP23008::write(uint32_t value)
{
  m_com->write_reg8(MCP23008_GPIO, value);
}

uint32_t MCP23008::read()
{ 
  m_com->write_reg8(MCP23008_OLAT);
  return m_com->read_reg8()&0xFF;
}

