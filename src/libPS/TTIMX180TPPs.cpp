#include "TTIMX180TPPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(TTIMX180TPPs)

TTIMX180TPPs::TTIMX180TPPs(const std::string& name) :
IPowerSupply(name, {"MX180TP"})
{ }

bool TTIMX180TPPs::ping()
{
  std::string result = m_com->sendreceive("*IDN?");
  return !result.empty();
}

void TTIMX180TPPs::reset()
{
  m_com->send("OPALL 0");
  m_com->send("*RST");

  if(!ping())
    throw std::runtime_error("No communication after reset.");
}

std::string TTIMX180TPPs::identify()
{
  std::string idn=m_com->sendreceive("*IDN?");
  return idn;
}

void TTIMX180TPPs::turnOn(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  m_com->send("OP"+std::to_string(channel)+" 1");
}

void TTIMX180TPPs::turnOff(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  m_com->send("OP"+std::to_string(channel)+" 0");
}

void TTIMX180TPPs::setCurrentLevel(double cur, unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  m_com->send("I" + std::to_string(channel)+" "+std::to_string(cur));
}

double TTIMX180TPPs::getCurrentLevel(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(m_com->sendreceive("I" + std::to_string(channel) + "?").substr(0,5));
}

void TTIMX180TPPs::setCurrentProtect(double maxcur, unsigned channel)
{
  setCurrentLevel(maxcur, channel);
}

double TTIMX180TPPs::getCurrentProtect(unsigned channel)
{
  return getCurrentLevel(channel);
}

double TTIMX180TPPs::measureCurrent(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(m_com->sendreceive("I" + std::to_string(channel) + "O?").substr(0,5));
}

void TTIMX180TPPs::setVoltageLevel(double volt, unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  m_com->send("V" + std::to_string(channel)+" "+std::to_string(volt));
}

double TTIMX180TPPs::getVoltageLevel(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(m_com->sendreceive("V" + std::to_string(channel) + "?").substr(0,5));
}

void TTIMX180TPPs::setVoltageProtect(double maxvolt, unsigned channel)
{
  setVoltageLevel(maxvolt, channel);
}

double TTIMX180TPPs::getVoltageProtect(unsigned channel)
{
  return getVoltageLevel(channel);
}

double TTIMX180TPPs::measureVoltage(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(m_com->sendreceive("V" + std::to_string(channel) + "O?").substr(0,5));
}
