#include "TTITSX1820PPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(TTITSX1820PPs)

TTITSX1820PPs::TTITSX1820PPs(const std::string& name) :
IPowerSupply(name, {"TSX1820P"})
{ }

bool TTITSX1820PPs::ping()
{
  std::string result = m_com->sendreceive("*IDN?");
  return !result.empty();
}

void TTITSX1820PPs::reset()
{
  m_com->send("OPALL 0");
  m_com->send("*RST");

  if(!ping())
    throw std::runtime_error("No communication after reset.");
}

std::string TTITSX1820PPs::identify()
{
  std::string idn=m_com->sendreceive("*IDN?");
  return idn;
}

void TTITSX1820PPs::turnOn(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  m_com->send("OP 1");
}

void TTITSX1820PPs::turnOff(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  m_com->send("OP 0");
}

void TTITSX1820PPs::setCurrentLevel(double cur, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  m_com->send("I "+std::to_string(cur));
}

double TTITSX1820PPs::getCurrentLevel(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  return std::stod(m_com->sendreceive("I?").substr(0,5));
}

void TTITSX1820PPs::setCurrentProtect(double maxcur, unsigned channel)
{
  setCurrentLevel(maxcur, channel);
}

double TTITSX1820PPs::getCurrentProtect(unsigned channel)
{
  return getCurrentLevel(channel);

}

double TTITSX1820PPs::measureCurrent(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  return std::stod(m_com->sendreceive("IO?").substr(0,5));
}

void TTITSX1820PPs::setVoltageLevel(double volt, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  m_com->send("V "+std::to_string(volt));
}

double TTITSX1820PPs::getVoltageLevel(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  return std::stod(m_com->sendreceive("V?").substr(0,5));
}

void TTITSX1820PPs::setVoltageProtect(double maxvolt, unsigned channel)
{
  setVoltageLevel(maxvolt, channel);
}

double TTITSX1820PPs::getVoltageProtect(unsigned channel)
{
  return getVoltageLevel(channel);
}

double TTITSX1820PPs::measureVoltage(unsigned channel)
{
  return std::stod(m_com->sendreceive("VO?").substr(0,5));
}
