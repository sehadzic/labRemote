#include "pybind11_json/pybind11_json.hpp"
#include "nlohmann/json.hpp"
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "IPowerSupply.h"
#include "SCPIPs.h"
#include "AgilentPs.h"
#include "AgilentE3631APs.h"
#include "AgilentE364xAPs.h"
#include "AgilentE3634APs.h"
#include "AgilentE3648APs.h"

#include "Bk16XXPs.h"
#include "DT54xxPs.h"
#include "Keithley22XX.h"
#include "Keithley24XX.h"
#include "PowerSupplyChannel.h"
#include "RS_HMPXXXX.h"
#include "RigolDP832.h"
#include "SorensenPs.h"
#include "TTIMX180TPPs.h"
#include "TTITSX1820PPs.h"

namespace py = pybind11;
namespace nl = nlohmann;

class PyIPowerSupply : public IPowerSupply {
public:
    /* Inherit the constructors */
    using IPowerSupply::IPowerSupply;

    void setConfiguration(const nl::json& config) override {
      PYBIND11_OVERLOAD(
          void,
          IPowerSupply,
          setConfiguration,
          config
      );
    }
    bool ping() override {
      PYBIND11_OVERLOAD_PURE(
          bool,
          IPowerSupply,
          ping,
      );
    }
    void checkCompatibilityList() override {
      PYBIND11_OVERLOAD(
        void,
        IPowerSupply,
        checkCompatibilityList,
      );
    }
    void reset() override {
      PYBIND11_OVERLOAD_PURE(
        void,
        IPowerSupply,
        reset,
      );
    }
    std::vector<std::string> getListOfModels() override {
      PYBIND11_OVERLOAD(
        std::vector<std::string>,
        IPowerSupply,
        getListOfModels,
      );
    }
    std::string identify() override {
      PYBIND11_OVERLOAD_PURE(
        std::string,
        IPowerSupply,
        identify,
      );
    }
    void turnOn(unsigned channel = 0) override {
      PYBIND11_OVERLOAD_PURE(
        void,
        IPowerSupply,
        turnOn,
        channel
      );
    }
    void turnOff(unsigned channel = 0) override {
      PYBIND11_OVERLOAD_PURE(
        void,
        IPowerSupply,
        turnOff,
        channel
      );
    }
    void setCurrentLevel(double cur, unsigned channel = 0) override {
      PYBIND11_OVERLOAD_PURE(
        void,
        IPowerSupply,
        setCurrentLevel,
        cur,
        channel
      );
    }
    double getCurrentLevel(unsigned channel = 0) override {
      PYBIND11_OVERLOAD_PURE(
        double,
        IPowerSupply,
        getCurrentLevel,
        channel
      );
    }
    void setCurrentProtect(double cur, unsigned channel = 0) override {
      PYBIND11_OVERLOAD_PURE(
        void,
        IPowerSupply,
        setCurrentProtect,
        cur,
        channel
      );
    }
    double getCurrentProtect(unsigned channel = 0) override {
      PYBIND11_OVERLOAD_PURE(
        double,
        IPowerSupply,
        getCurrentProtect,
        channel
      );
    }
    double measureCurrent(unsigned channel = 0) override {
      PYBIND11_OVERLOAD_PURE(
        double,
        IPowerSupply,
        measureCurrent,
        channel
      );
    }
    void setVoltageLevel(double volt, unsigned channel = 0) override {
      PYBIND11_OVERLOAD_PURE(
        void,
        IPowerSupply,
        setVoltageLevel,
        volt,
        channel
      );
    }
    double getVoltageLevel(unsigned channel = 0) override {
      PYBIND11_OVERLOAD_PURE(
        double,
        IPowerSupply,
        getVoltageLevel,
        channel
      );
    }
    void setVoltageProtect(double volt, unsigned channel = 0) override {
      PYBIND11_OVERLOAD_PURE(
        void,
        IPowerSupply,
        setVoltageProtect,
        volt,
        channel
      );
    }
    double getVoltageProtect(unsigned channel = 0) override {
      PYBIND11_OVERLOAD_PURE(
        double,
        IPowerSupply,
        getVoltageProtect,
        channel
      );
    }
    double measureVoltage(unsigned channel = 0) override {
      PYBIND11_OVERLOAD_PURE(
        double,
        IPowerSupply,
        measureVoltage,
        channel
      );
    }

};

// template trampoline to reduce code duplication
template <class PSBase = AgilentPs> class PyPS: public PSBase {
public:
    /* Inherit the constructors */
    using PSBase::PSBase;

    bool ping() override {
      PYBIND11_OVERLOAD(
        bool,
        PSBase,
        ping,
      );
    }
    std::string identify() override {
      PYBIND11_OVERLOAD(
        std::string,
        PSBase,
        identify,
      );
    }
    void reset() override {
      PYBIND11_OVERLOAD(
        void,
        PSBase,
        reset,
      );
    }
    void turnOn(unsigned channel) override {
      PYBIND11_OVERLOAD(
        void,
        PSBase,
        turnOn,
        channel
      );
    }
    void turnOff(unsigned channel) override {
      PYBIND11_OVERLOAD(
        void,
        PSBase,
        turnOff,
        channel
      );
    }
    void setCurrentLevel(double cur, unsigned channel = 0) override {
      PYBIND11_OVERLOAD(
        void,
        PSBase,
        setCurrentLevel,
        cur,
        channel
      );
    }
    double getCurrentLevel(unsigned channel = 0) override {
      PYBIND11_OVERLOAD(
        double,
        PSBase,
        getCurrentLevel,
        channel
      );
    }
    void setCurrentProtect(double cur, unsigned channel = 0) override {
      PYBIND11_OVERLOAD(
        void,
        PSBase,
        setCurrentProtect,
        cur,
        channel
      );
    }
    double getCurrentProtect(unsigned channel = 0) override {
      PYBIND11_OVERLOAD(
        double,
        PSBase,
        getCurrentProtect,
        channel
      );
    }
    double measureCurrent(unsigned channel = 0) override {
      PYBIND11_OVERLOAD(
        double,
        PSBase,
        measureCurrent,
        channel
      );
    }
    void setVoltageLevel(double volt, unsigned channel = 0) override {
      PYBIND11_OVERLOAD(
        void,
        PSBase,
        setVoltageLevel,
        volt,
        channel
      );
    }
    double getVoltageLevel(unsigned channel = 0) override {
      PYBIND11_OVERLOAD(
        double,
        PSBase,
        getVoltageLevel,
        channel
      );
    }
    void setVoltageProtect(double volt, unsigned channel = 0) override {
      PYBIND11_OVERLOAD(
        void,
        PSBase,
        setVoltageProtect,
        volt,
        channel
      );
    }
    double getVoltageProtect(unsigned channel = 0) override {
      PYBIND11_OVERLOAD(
        double,
        PSBase,
        getVoltageProtect,
        channel
      );
    }
    double measureVoltage(unsigned channel = 0) override {
      PYBIND11_OVERLOAD(
        double,
        PSBase,
        measureVoltage,
        channel
      );
    }
};



void register_ps(py::module& m){
  py::class_<IPowerSupply, PyIPowerSupply, std::shared_ptr<IPowerSupply>>(m, "IPowerSupply")
    .def(py::init<const std::string &>())
    .def("setCom", &IPowerSupply::setCom)
    .def("setConfiguration", &IPowerSupply::setConfiguration)
    .def("getConfiguration", &IPowerSupply::getConfiguration)
    .def("ping", &IPowerSupply::ping)
    .def("checkCompatibilityList", &IPowerSupply::checkCompatibilityList)
    .def("reset", &IPowerSupply::reset)
    .def("getListOfModels", &IPowerSupply::getListOfModels)
    .def("identify", &IPowerSupply::identify)
    .def("turnOn", &IPowerSupply::turnOn,
         py::arg("channel") = 0)
    .def("turnOff", &IPowerSupply::turnOff,
         py::arg("channel") = 0)
    .def("setCurrentLevel", &IPowerSupply::setCurrentLevel,
         py::arg("current"), py::arg("channel") = 0)
    .def("getCurrentLevel", &IPowerSupply::getCurrentLevel,
         py::arg("channel") = 0)
    .def("setCurrentProtect", &IPowerSupply::setCurrentProtect,
         py::arg("current"), py::arg("channel") = 0)
    .def("getCurrentProtect", &IPowerSupply::getCurrentProtect,
         py::arg("channel") = 0)
    .def("measureCurrent", &IPowerSupply::measureCurrent,
         py::arg("channel") = 0)
    .def("setVoltageLevel", &IPowerSupply::setVoltageLevel,
         py::arg("voltage"), py::arg("channel") = 0)
    .def("getVoltageLevel", &IPowerSupply::getVoltageLevel,
         py::arg("channel") = 0)
    .def("setVoltageProtect", &IPowerSupply::setVoltageProtect,
         py::arg("voltage"), py::arg("channel") = 0)
    .def("getVoltageProtect", &IPowerSupply::getVoltageProtect,
         py::arg("channel") = 0)
    .def("measureVoltage", &IPowerSupply::measureVoltage,
         py::arg("channel") = 0);

  py::class_<SCPIPs, PyPS<SCPIPs>, IPowerSupply, std::shared_ptr<SCPIPs>>(m, "SCPIPs")
    .def(py::init<const std::string&, std::vector<std::string>&, unsigned>(),
         py::arg("name"), py::arg("models") = py::list(), py::arg("maxChannels") = 0
    );

  py::class_<AgilentPs, SCPIPs, std::shared_ptr<AgilentPs>>(m, "AgilentPs")
    .def(py::init<const std::string&, std::vector<std::string>&, unsigned>(),
         py::arg("name"), py::arg("models") = py::list(), py::arg("maxChannels") = 0
    )
    .def("beepOff", &AgilentPs::beepOff);

  py::class_<AgilentE3631APs, AgilentPs, std::shared_ptr<AgilentE3631APs>>(m, "AgilentE3631APs")
    .def(py::init<const std::string &>());

  py::class_<AgilentE364xAPs, AgilentPs, std::shared_ptr<AgilentE364xAPs>>(m, "AgilentE364xAPs")
    .def(py::init<const std::string &>());

  py::class_<AgilentE3634APs, AgilentPs, std::shared_ptr<AgilentE3634APs>>(m, "AgilentE3634APs")
    .def(py::init<const std::string &>());

  py::class_<AgilentE3648APs, AgilentPs, std::shared_ptr<AgilentE3648APs>>(m, "AgilentE3648APs")
    .def(py::init<const std::string &>());

  py::class_<Bk16XXPs, PyPS<Bk16XXPs>, IPowerSupply, std::shared_ptr<Bk16XXPs>>(m, "Bk16XXPs")
    .def(py::init<const std::string &>());

  py::class_<DT54xxPs, PyPS<DT54xxPs>, IPowerSupply, std::shared_ptr<DT54xxPs>> py_DT54xxPs(m, "DT54xxPs");

  py_DT54xxPs
    .def(py::init<const std::string &>())
    .def("status", &DT54xxPs::status);

  py::enum_<DT54xxPs::Status>(py_DT54xxPs, "Status")
    .value("On", DT54xxPs::Status::On)
    .value("RampingUp", DT54xxPs::Status::RampingUp)
    .value("RampingDown", DT54xxPs::Status::RampingDown)
    .value("OVC", DT54xxPs::Status::OVC)
    .value("OVV", DT54xxPs::Status::OVV)
    .value("UNV", DT54xxPs::Status::UNV)
    .value("MAXV", DT54xxPs::Status::MAXV)
    .value("Trip", DT54xxPs::Status::Trip)
    .value("OVT", DT54xxPs::Status::OVT)
    .value("Disabled", DT54xxPs::Status::Disabled)
    .value("Kill", DT54xxPs::Status::Kill)
    .value("Interlock", DT54xxPs::Status::Interlock)
    .value("CalError", DT54xxPs::Status::CalError);


  py::class_<Keithley22XX, PyPS<Keithley22XX>, IPowerSupply, std::shared_ptr<Keithley22XX>>(m, "Keithley22XX")
    .def(py::init<const std::string &>());

  py::class_<Keithley24XX, PyPS<Keithley24XX>, IPowerSupply, std::shared_ptr<Keithley24XX>>(m, "Keithley24XX")
    .def(py::init<const std::string &>());


  py::class_<RS_HMPXXXX, SCPIPs, std::shared_ptr<RS_HMPXXXX>>(m, "RS_HMPXXXX")
    .def(py::init<const std::string &>());


  py::class_<RigolDP832, SCPIPs, std::shared_ptr<RigolDP832>>(m, "RigolDP832")
    .def(py::init<const std::string &>());


  py::class_<SorensenPs, PyPS<SorensenPs>, IPowerSupply, std::shared_ptr<SorensenPs>>(m, "SorensenPs")
    .def(py::init<const std::string &>());


  py::class_<TTIMX180TPPs, PyPS<TTIMX180TPPs>, IPowerSupply, std::shared_ptr<TTIMX180TPPs>>(m, "TTIMX180TPPs")
    .def(py::init<const std::string &>());

  py::class_<TTITSX1820PPs, PyPS<TTITSX1820PPs>, IPowerSupply, std::shared_ptr<TTITSX1820PPs>>(m, "TTITSX1820PPs")
    .def(py::init<const std::string &>());

  py::class_<PowerSupplyChannel, std::shared_ptr<PowerSupplyChannel>>(m, "PowerSupplyChannel")
    .def(py::init<const std::string &, std::shared_ptr<IPowerSupply>, unsigned>())
    .def("getName", &PowerSupplyChannel::getName)
    .def("getPowerSupply", &PowerSupplyChannel::getPowerSupply)
    .def("getChannel", &PowerSupplyChannel::getChannel)
    .def("setProgram", &PowerSupplyChannel::setProgram)
    .def("program", &PowerSupplyChannel::program)
    .def("turnOn", &PowerSupplyChannel::turnOn)
    .def("turnOff", &PowerSupplyChannel::turnOff)
    .def("setCurrentLevel", &PowerSupplyChannel::setCurrentLevel)
    .def("getCurrentLevel", &PowerSupplyChannel::getCurrentLevel)
    .def("setCurrentProtect", &PowerSupplyChannel::setCurrentProtect)
    .def("measureCurrent", &PowerSupplyChannel::measureCurrent)
    .def("setVoltageLevel", &PowerSupplyChannel::setVoltageLevel)
    .def("getVoltageLevel", &PowerSupplyChannel::getVoltageLevel)
    .def("setVoltageProtect", &PowerSupplyChannel::setVoltageProtect)
    .def("measureVoltage", &PowerSupplyChannel::measureVoltage);
}
