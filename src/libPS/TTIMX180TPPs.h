#ifndef TTIMX180TPPS_H
#define TTIMX180TPPS_H

#include <chrono>
#include <memory>
#include <string>

#include "IPowerSupply.h"

#include "SerialCom.h"

/** \brief TTi MX180TPP
 *
 * Implementation for the TTi MX180TPP power supply.
 *
 * [Progamming Manual](http://resources.aimtti.com/manuals/MX180T+MX180TP_Instruction_Manual-Iss5.pdf)
 *
 * Other TTi power supplies might be supported too, but
 * have not been checked.
 */
class TTIMX180TPPs : public IPowerSupply
{
public:
  TTIMX180TPPs(const std::string& name);
  ~TTIMX180TPPs() =default;

  /** \name Communication
   * @{
   */

  virtual bool ping();

  virtual std::string identify();

  /** @} */

  /** \name Power Supply Control
   * @{
   */

  virtual void reset();
  virtual void turnOn(unsigned channel);
  virtual void turnOff(unsigned channel);

  /** @} */
  
  /** \name Current Control and Measurement
   * @{
   */

  virtual void   setCurrentLevel(double cur, unsigned channel = 0);
  virtual double getCurrentLevel(unsigned channel = 0);
  virtual void   setCurrentProtect(double maxcur , unsigned channel = 0);
  virtual double getCurrentProtect(unsigned channel = 0);  
  virtual double measureCurrent(unsigned channel = 0);

  /** @} */

  /** \name Voltage Control and Measurement
   * @{
   */

  virtual void   setVoltageLevel(double volt, unsigned channel = 0);
  virtual double getVoltageLevel(unsigned channel = 0);
  virtual void   setVoltageProtect(double maxvolt , unsigned channel = 0 );
  virtual double getVoltageProtect(unsigned channel = 0);
  virtual double measureVoltage(unsigned channel = 0);

  /** @} */
};

#endif

