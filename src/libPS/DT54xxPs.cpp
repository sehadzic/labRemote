#include "DT54xxPs.h"

#include "TextSerialCom.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(DT54xxPs)

DT54xxPs::DT54xxPs(const std::string& name) :
IPowerSupply(name, {"DT5472"})
{ }

void DT54xxPs::reset()
{
  command("SET","OFF");
  command("SET","BDCLR");

  std::string result=identify();
  if(result.empty())
    throw std::runtime_error("No communication after reset.");
}

std::string DT54xxPs::identify()
{
  std::string idn=command("MON","BDNAME");
  return idn;
}
  
bool DT54xxPs::ping()
{
  std::string result=command("MON","BDNAME");
  return !result.empty();
}

void DT54xxPs::turnOn(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  command("SET","ON");

  while(status(channel)&Status::RampingUp)
    {
      double volt=measureVoltage();
      logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> ramping up: " << volt << "V";
      std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}

void DT54xxPs::turnOff(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  command("SET","OFF");

  while(status(channel)&Status::RampingDown)
    {
      double volt=measureVoltage();
      logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> ramping down: " << volt << "V";
      std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}

void DT54xxPs::setCurrentLevel(double cur, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  command("SET", "ISET", std::to_string(cur*1e6));
}

double DT54xxPs::getCurrentLevel(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");
  return std::stod(command("MON","ISET"))/1e6;
}

void DT54xxPs::setCurrentProtect(double maxcur, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");
  setCurrentLevel(maxcur, channel);
}

double DT54xxPs::getCurrentProtect(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  return getCurrentLevel(channel);
}

double DT54xxPs::measureCurrent(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  return std::stod(command("MON","IMON"))/1e6;
}

void DT54xxPs::setVoltageLevel(double volt, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  command("SET", "VSET", std::to_string(volt));
}

double DT54xxPs::getVoltageLevel(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  return std::stod(command("MON","VSET"));
}

void DT54xxPs::setVoltageProtect(double maxvolt, unsigned channel)
{

  setVoltageLevel(maxvolt, channel);

}

double DT54xxPs::getVoltageProtect(unsigned channel)
{

  return getVoltageLevel(channel);
}

double DT54xxPs::measureVoltage(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");
  return std::stod(command("MON","VMON"));
}

uint16_t DT54xxPs::status(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  return std::stoi(command("MON","STAT"))&0xFFFF;
}

std::string DT54xxPs::command(const std::string& cmd, const std::string& par, const std::string& value)
{
  // Build command
  std::string tosend="$CMD:"+cmd+",PAR:"+par;
  if(!value.empty())
    tosend+=",VAL:"+value;

  // Send command and receive response
  std::string resp=m_com->sendreceive(tosend);

  // Parse response
  if(resp.empty())
    throw "DT54xx: No response :(";

  std::string retvalue;
  std::string cmdvalue;
  
  std::string token;
  std::stringstream ss(resp);
  while(std::getline(ss, token, ','))
    {
      size_t seppos=token.find(':');
      if(seppos==std::string::npos)
	continue; // Not a valid part
      if(token.substr(0,seppos)=="VAL")
	{ // This is the value part!
	  retvalue=token.substr(seppos+1);
	}
      else if(token.substr(0,seppos)=="#CMD")
	{ // This is the value part!
	  cmdvalue=token.substr(seppos+1);
	}
    }

  if(cmdvalue.empty())
    throw "DT54xx: No CMD in return statement :(";

  if(cmdvalue=="ERR")
    throw "DT54xx:: CMD shows an error :(";

  return retvalue;
}

