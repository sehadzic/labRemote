#ifndef TTITSX1820PPS_H
#define TTITSX1820PPS_H

// ####################
// TTi Power supply
// Author: Neha Santpur
// Date: Sep 2018
// Notes: Assuming Prologix GPIB to USB
// ###################

#include <chrono>
#include <memory>
#include <string>

#include "IPowerSupply.h"

#include "SerialCom.h"

/** \brief TTi TSX1820P
 *
 * Implementation for the TTi TSX1820P power supply.
 *
 * [Progamming Manual](http://resources.aimtti.com/manuals/TSX-P_Instruction_Manual-Iss16.pdf)
 *
 * Other TTi power supplies might be supported too, but
 * have not been checked.
 */
class TTITSX1820PPs : public IPowerSupply
{
public:
  TTITSX1820PPs(const std::string& name);
  ~TTITSX1820PPs() =default;

  /** \name Communication
   * @{
   */

  virtual bool ping();

  virtual std::string identify();

  /** @} */

  /** \name Power Supply Control
   * @{
   */

  virtual void reset();
  virtual void turnOn(unsigned channel);
  virtual void turnOff(unsigned channel);

  /** @} */
  
  /** \name Current Control and Measurement
   * @{
   */

  virtual void   setCurrentLevel(double cur, unsigned channel = 0);
  virtual double getCurrentLevel(unsigned channel = 0);
  virtual void   setCurrentProtect(double maxcur , unsigned channel = 0);
  virtual double getCurrentProtect(unsigned channel = 0);  
  virtual double measureCurrent(unsigned channel = 0);

  /** @} */

  /** \name Voltage Control and Measurement
   * @{
   */

  virtual void   setVoltageLevel(double volt, unsigned channel = 0);
  virtual double getVoltageLevel(unsigned channel = 0);
  virtual void   setVoltageProtect(double maxvolt , unsigned channel = 0 );
  virtual double getVoltageProtect(unsigned channel = 0);
  virtual double measureVoltage(unsigned channel = 0);

  /** @} */
};

#endif

