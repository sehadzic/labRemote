#include <pybind11/pybind11.h>

namespace py = pybind11;

void register_com(py::module&);
void register_ps(py::module&);
void register_equipconf(py::module&);

PYBIND11_MODULE(labRemote, m) {
    py::module com = m.def_submodule("com");
    register_com(com);

    py::module ps = m.def_submodule("ps");
    register_ps(ps);

    py::module ec = m.def_submodule("ec");
    register_equipconf(ec);
}
