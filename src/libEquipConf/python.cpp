#include "pybind11_json/pybind11_json.hpp"
#include "nlohmann/json.hpp"
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "EquipConf.h"

namespace py = pybind11;
namespace nl = nlohmann;

void register_equipconf(py::module& m){
  py::class_<EquipConf>(m, "EquipConf")
    .def(py::init<>())
    .def(py::init<const std::string &>())
    .def(py::init<const nl::json &>())
    .def("setHardwareConfig", (void (EquipConf::*)(const std::string&)) &EquipConf::setHardwareConfig)
    .def("setHardwareConfig", (void (EquipConf::*)(const nl::json&)) &EquipConf::setHardwareConfig)
    .def("getDeviceConf", &EquipConf::getDeviceConf)
    .def("getChannelConf", &EquipConf::getChannelConf)
    .def("getPowerSupply", &EquipConf::getPowerSupply)
    .def("getPowerSupplyChannel", &EquipConf::getPowerSupplyChannel);
}
